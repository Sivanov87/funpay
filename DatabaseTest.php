<?php

namespace FpDbTest;

use Exception;

class DatabaseTest
{
    private DatabaseInterface $db;

    public function __construct(DatabaseInterface $db)
    {
        $this->db = $db;
    }

    public function testBuildQuery(): void
    {
        $results = [];

        $results[] = $this->db->buildQuery('SELECT name FROM users WHERE user_id = 1');

        $results[] = $this->db->buildQuery(
            'SELECT * FROM users WHERE name = ? AND block = 0',
            ['Jack']
        );

        $results[] = $this->db->buildQuery(
            'SELECT ?# FROM users WHERE user_id = ?d AND block = ?d',
            [['name', 'email'], 2, true]
        );

        $results[] = $this->db->buildQuery(
            'UPDATE users SET ?a WHERE user_id = -1',
            [['name' => 'Jack', 'email' => null]]
        );

        foreach ([null, true] as $block) {
            $results[] = $this->db->buildQuery(
                'SELECT name FROM users WHERE ?# IN (?a){ AND block = ?d}',
                ['user_id', [1, 2, 3], $block ?? $this->db->skip()]
            );
        }

        $correct = [
            'SELECT name FROM users WHERE user_id = 1',
            'SELECT * FROM users WHERE name = \'Jack\' AND block = 0',
            'SELECT `name`, `email` FROM users WHERE user_id = 2 AND block = 1',
            'UPDATE users SET `name` = \'Jack\', `email` = NULL WHERE user_id = -1',
            'SELECT name FROM users WHERE `user_id` IN (1, 2, 3)',
            'SELECT name FROM users WHERE `user_id` IN (1, 2, 3) AND block = 1',
        ];

        if ($results !== $correct) {
          throw new Exception('Failure.');
        }
    }

    public function testBuildQueryIncorrect(): void
    {
      $queries = [];

      $vals = [
        '?'  => [['bar'], new \stdClass(), ['bar' => 'foo']],
        '?f' => ['foo', false, ['bar'], new \stdClass()],
        '?d' => ['foo', 9.9, ['bar'], new \stdClass()],
        '?a' => [false, 9.9, 15, 'bar', [['bar']], new \stdClass()],
        '?#' => [false, 9.9, 15, [['bar']], new \stdClass(), '', ['']],
      ];

      foreach($vals as $p => $values) {
        foreach ($values as $val) {
          $queries[] = ['SELECT * FROM users WHERE user_id = ' . $p, [$val]];
        }
      }

      $queries[] = ['SELECT * FROM users WHERE {user_id = {}', []];
      $queries[] = ['SELECT * FROM users WHERE user_id = 1', [2]];
      $queries[] = ['SELECT * FROM users WHERE user_id = ?d', [2, 3]];
      $queries[] = ['SELECT * FROM users WHERE user_id = ?', []];

      foreach($queries as $query) {
        $ok = false;
        try {
          $this->db->buildQuery($query[0], $query[1]);
          $ok = true;
        } catch(Exception $e) {

        }

        if ($ok) {
          throw new Exception('Failure: ' . $query[0] . ' - ' . json_encode($query[1]));
        }
      }
    }

  public function testBuildQueryHard(): void
  {
    $results = [];

    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = "? ?# {{}\' \\" " AND user_id = 1');
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ?}{ AND item = ?}', [$this->db->skip(), $this->db->skip()]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ?}{ AND item = ?}', [$this->db->skip(), 1]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ?}{ AND item = ?}', [3, $this->db->skip()]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ? AND item = ?}', [3, $this->db->skip()]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ? AND item = ?}', [$this->db->skip(), 2]);
    $results[] = $this->db->buildQuery('SELECT ?# FROM users WHERE 1', [['na`me']]);
    $results[] = $this->db->buildQuery('UPDATE users SET ?a WHERE user_id = -1', [['na`me' => 'Jack']]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE user_id = ?f', [1.0E-13]);
    $results[] = $this->db->buildQuery('SELECT ?#, ?# FROM users WHERE ?# IN (?a, ?)', ['user_id', ["block", "name"], "user_id", [1, "bar", null, 1.1, false, true], 3]);
    $results[] = $this->db->buildQuery('SELECT name FROM users WHERE text = 1{ AND block = ?d}', [$this->db->skip()]);

    $correct = ['SELECT name FROM users WHERE text = "? ?# {{}\' \\" " AND user_id = 1'];
    $correct[] = 'SELECT name FROM users WHERE text = 1';
    $correct[] = 'SELECT name FROM users WHERE text = 1 AND item = 1';
    $correct[] = 'SELECT name FROM users WHERE text = 1 AND block = 3';
    $correct[] = 'SELECT name FROM users WHERE text = 1';
    $correct[] = 'SELECT name FROM users WHERE text = 1';
    $correct[] = 'SELECT `name` FROM users WHERE 1';
    $correct[] = 'UPDATE users SET `name` = \'Jack\' WHERE user_id = -1';
    $correct[] = 'SELECT name FROM users WHERE user_id = 1.0E-13';
    $correct[] = 'SELECT `user_id`, `block`, `name` FROM users WHERE `user_id` IN (1, \'bar\', NULL, 1.1, 0, 1, 3)';
    $correct[] = 'SELECT name FROM users WHERE text = 1';

    if ($results !== $correct) {
      throw new Exception('Failure.');
    }
  }
}
