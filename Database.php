<?php

namespace FpDbTest;

use Exception;
use mysqli;

class Skip {

};

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
      $vars_index = 0; // ������ �������� ��������� �������
      $result = ''; // ������������� ������
      $if_substring = null; // ������� ��������� ��������� �����
      $need_skip_substring = false; // ������������� ���������� ��������� �������� �����
      $wait_end_string_symbol = null; // ����� ������ ����� ������ �� ������� �� ������ �����
      // ���������� ��� ������� ������
      for ($index = 0; $index < strlen($query); $index++) {
        $ch = $query[$index];
        $is_backslashes = $index > 0 && $query[$index-1] == '\\';
        if (!is_null($wait_end_string_symbol)) {
          // ��������� ���������� �������� ������ �������
          if (!is_null($if_substring)) {
            $if_substring .= $ch;
          } else {
            $result .= $ch;
          }

          if (!$is_backslashes && $wait_end_string_symbol == $ch) {
            $wait_end_string_symbol = null;
          }
        } else if ($ch == '{') {
          // ������������� ��������� �����
          if (!is_null($if_substring)) {
            // �� ����� ���� ��������� �������� ������
            throw new Exception('Nested blocks');
          }
          $if_substring = '';
        } else if ($ch == '}') {
          // ��������� ��������� ��������� �����
          if (!$need_skip_substring) {
            $result .= $if_substring;
          }
          $if_substring = null;
          $need_skip_substring = false;
        } else if ($ch == '?') {
          // ��������� ������������ ���������
          $var_pattern = '?';
          if ($index < strlen($query) - 1) {
            if (in_array($query[$index+1], ['a', 'f', 'd', '#'])) {
              $var_pattern .= $query[$index+1];
              $index++;
            }
          }
          if (count($args) <= $vars_index) {
            // ���� ����������
            throw new Exception('Too few args');
          }

          $arg = $args[$vars_index];

          if (!is_null($if_substring) && !$need_skip_substring) {
            // ���������, �� ����� �� ���������� ���� �������� ����
            $is_skip = $this->checkSkip($arg);
            if ($is_skip) {
              $need_skip_substring = true;
            }
          }
          if (is_null($if_substring) || !$need_skip_substring) {
            $value = $this->getVarsValue($var_pattern, $arg);
            if (!is_null($if_substring)) {
              $if_substring .= $value;
            } else {
              $result .= $value;
            }
          }
          $vars_index++;
        } else {
          // ������ �������
          if (!is_null($if_substring)) {
            $if_substring .= $ch;
          } else {
            $result .= $ch;
          }

          if (!$is_backslashes && in_array($ch, ['\'', '"'])) {
            // ��������� ������ ������ ������ �������
            $wait_end_string_symbol = $ch;
          }
        }
      }

      if (!is_null($if_substring)) {
        // ������������ �������� ����
        throw new Exception('If-block does not finished');
      }

      if (count($args) !== $vars_index) {
        // ������� ����� ����������
        throw new Exception('Too much args');
      }

      return $result;
    }

    public function skip()
    {
        return new Skip();
    }

    // �������� ����, ��� ��� �������� �������� ��������� ��� ��������
    private function checkSkip($var): bool
    {
      return is_object($var) && get_class($var) == Skip::class;
    }

    // ����������� ������������ � ������ �������� ��������� � ������ ��� �������������
    private function getVarsValue($var_pattern, $arg): string {
      if ($var_pattern == '?d') {
        if (is_int($arg)) {
          return (string)$arg;
        } elseif (is_bool($arg)) {
          return $arg ? '1' : '0';
        } elseif(is_null($arg)) {
          return 'NULL';
        } else {
          // ������������ ��������
          throw new Exception('Incorrect ?d value');
        }
      }

      if ($var_pattern == '?f') {
        // ��� � �������� float ���������� � ���������� ���� int, �� �� ���������� bool. �������, �� ������ ���� �������
        if (is_int($arg) || is_float($arg)) {
          return (string)$arg;
        } elseif(is_null($arg)) {
          return 'NULL';
        } else {
          // ������������ ��������
          throw new Exception('Incorrect ?f value');
        }
      }

      if ($var_pattern == '?') {
        return $this->getCommonArg($arg);
      }

      if ($var_pattern == '?#') {
        if (is_string($arg)) {
          $arg = [$arg];
        }
        if (!is_array($arg)) {
          // ������������ ��������
          throw new Exception('Incorrect ?# value');
        }
        $result = [];
        foreach ($arg as $item) {
          if (is_string($item) && strlen($item) > 0) {
            $result[] = '`' . $this->escapeFieldName($item) . '`';
          } else {
            // ���������������� ����� ���� ������ �������� ������
            throw new Exception('Incorrect ?# item');
          }
        }

        return implode(', ', $result);
      }

      if ($var_pattern == '?a') {
        if (!is_array($arg)) {
          throw new Exception('Incorrect ?a value');
        }
        $result = [];
        $is_assoc = null;
        foreach ($arg as $key => $item) {
          if (is_null($is_assoc)) {
            $is_assoc = is_string($key);
          }
          // ��� �������� ������ ���� ��� ������� �������� ��� ������������� �� ���������� �������
          if ($is_assoc && !is_string($key)) {
            throw new Exception();
          }
          if (!$is_assoc && !is_int($key)) {
            throw new Exception();
          }

          $value = $this->getCommonArg($item);

          if ($is_assoc) {
            $result[] = '`' . $this->escapeFieldName($key) . '` = ' . $value;
          } else {
            $result[] = $value;
          }
        }

        return implode(', ', $result);
      }

      throw new Exception('Unknown variable type');
    }

    // ��������� ��������� ��� ������ ������������� ?
    private function getCommonArg($arg): string {
      if (is_int($arg) || is_float($arg)) {
        return (string)$arg;
      } if (is_bool($arg)) {
        return $arg ? '1' : '0';
      } else if (is_string($arg)) {
        return '\'' . $this->mysqli->real_escape_string($arg) . '\'';
      } else if (is_null($arg)) {
        return 'NULL';
      }

      throw new Exception('Incorrect ? value');
    }

  // ������� ��������������
  private function escapeFieldName($field_name): string {
    $field_name = $this->mysqli->real_escape_string($field_name);
    $field_name = str_replace("`", "", $field_name);

    return $field_name;
  }
}
